import React, { Component } from 'react'
import { Container, Row } from 'react-bootstrap'
import Square from './Square'

class Board extends Component {
  renderSquare (i) {
    let styleClass = this.props.styleClass ? this.styleClass : ``
    if (this.props.indexsWin.includes(i))
      styleClass = `${styleClass} square-highlight`

    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
        styleClass={styleClass}
        key={`col-${i}`}
      />
    )
  }

  render () {
    return (
      <Container>
        {[0, 1, 2].map(row => {
          return (
            <Row className='board-row' key={`row-${row}`}>
              {[0, 1, 2].map(col => {
                return this.renderSquare(3 * row + col)
              })}
            </Row>
          )
        })}
      </Container>
    )
  }
}

export default Board
