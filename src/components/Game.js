import React, { Component } from 'react'
import { Container, Row, Col, Button } from 'react-bootstrap'
import Board from './Board'

class Game extends Component {
  constructor (props) {
    super(props)
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true,
      orderIsAsc: true,
      hasReverse: false,
      indexsWin: []
    }
    this.handleClickOrder = this.handleClickOrder.bind(this)
  }

  calculateWinner (squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ]

    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i]
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return { winner: squares[a], stateWin: lines[i] }
      }
    }

    return null
  }

  updateIndexsWin (stateWin) {
    this.setState({
      indexsWin: stateWin
    })
  }

  handleClickOrder () {
    const history = this.state.history
    let newHistory
    if (this.state.hasReverse || this.state.orderIsAsc) {
      newHistory = history.reverse()
    } else {
      newHistory = history
    }

    this.setState({
      history: newHistory,
      orderIsAsc: !this.state.orderIsAsc,
      hasReverse: !this.state.hasReverse ? true : this.state.hasReverse
    })
  }

  handleClick (i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1)
    const current = this.state.orderIsAsc
      ? history[history.length - 1]
      : history[0]
    const squares = current.squares.slice()
    let result = this.calculateWinner(squares)

    //if has win or who clicked!!!
    if (result || squares[i]) return

    squares[i] = this.state.xIsNext ? 'X' : 'O'

    const newHistory = this.state.orderIsAsc
      ? history.concat([{ squares: squares }])
      : [{ squares: squares }].concat(history)

    result = this.calculateWinner(squares)

    this.setState({
      history: newHistory,
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
      indexsWin: result ? result.stateWin : []
    })
  }

  handleClickHistory (i) {
    return
  }

  jumpTo (step) {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0
    })
  }

  isCurrentStep (step, message) {
    if (step === this.state.stepNumber) return <b>{message}</b>
    return <span>{message}</span>
  }

  render () {
    const history = this.state.history
    const current = history[this.state.orderIsAsc ? this.state.stepNumber : 0]
    const result = this.calculateWinner(current.squares)
    const indexsWin = this.state.indexsWin
    const orderIsAsc = this.state.orderIsAsc

    //element of history list
    const moves = history.map((index, move) => {
      const desc = move ? `Go to move # ${move}` : `Go to game start`
      const stateWin =
        (orderIsAsc && move === history.length - 1) ||
        (!orderIsAsc && move === 0)
          ? indexsWin
          : []

      return (
        <li key={move} className='mb-2'>
          <div className='row'>
            <div className='col'>
              <Board
                squares={history[move].squares}
                onClick={this.handleClickHistory}
                styleClass='can-not-click'
                indexsWin={stateWin}
              ></Board>
            </div>
            <div className='col'>
              <Button
                variant='primary'
                size='sm'
                className='btn-block'
                onClick={() => this.jumpTo(move)}
              >
                {this.isCurrentStep(move, desc)}
              </Button>
            </div>
          </div>
        </li>
      )
    })

    let status
    if (result) status = `Winner : ${result.winner} !!`
    else if (this.state.stepNumber === 9) status = `Draw !!`
    else status = `Next player : ${this.state.xIsNext ? `X` : `O`}`

    return (
      <Container>
        <Row className='pt-4'>
          <Col md={8}>
            <h1>{`OX Game`}</h1>
            <Board
              squares={current.squares}
              onClick={i => this.handleClick(i)}
              indexsWin={indexsWin}
            />
          </Col>
          <Col md={4}>
            <h2>{status}</h2>
            <Row className='m-0 mb-2 justify-content-end'>
              <Button variant='success' onClick={this.handleClickOrder}>
                {this.state.orderIsAsc ? `Ascending` : `Descending`}
              </Button>
            </Row>
            <ol>{moves}</ol>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default Game
